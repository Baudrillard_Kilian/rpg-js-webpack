import '../style/style.scss';
import { Wizzard } from './classes/wizzard';
import { Clerc } from './classes/clerc';
import { Necromancer } from './classes/necromancer';
import { Warrior } from './classes/warrior';
import { Thief } from './classes/thief';
import { Personnage } from './personnages/createCharacters';

class Game {
    wizzards = new Wizzard;

    start(){
        const perso = new Personnage;
        console.log('game started..')

        const gandalf = this.wizzards;
        gandalf.name = 'Gandalf';
        gandalf.arme = gandalf.weapon.sceptre;
        perso.createWizzard(gandalf);
        console.log(gandalf);

        const luc = new Clerc;
        luc.name = 'Luc';
        luc.arme = luc.weapon.sceptre;
        console.log(luc);

        const rades = new Necromancer;
        rades.name = 'Rades Spirito';
        rades.arme = rades.weapon.grimoire;
        console.log(rades);

        const garen = new Warrior;
        garen.name = 'Garen';
        garen.arme = garen.heavyWeapon.epee2main;
        console.log(garen);

        const kazuma = new Thief;
        kazuma.name = 'Kazuma Sato';
        kazuma.arme = kazuma.weapon.poignard;
        console.log(kazuma);

    }

    
}

const game = new Game;
export default game;