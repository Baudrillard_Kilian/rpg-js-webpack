import { Global } from '../stats/global';
import { Heavy } from '../armes/heavy_weapon';

export class Warrior extends Global {
    heavyWeapon = new Heavy;
    rage = 0;

    agi = 3;
    armor = 5;
    speed = 2;
    str = 5;

    breakDown(){
        /*
        warrior attaque = -2 rage

        sort et attaque enemie = +5
            si parrer ou dodger = +3
        */
       // si mode furie attiver(rage = 100) alors l'armor et la str est doubler
       // et furie diminue de 20/tour et n'augmente pas avant la fin du break_down
        console.log(`${this.name} rentre en mode furie`);
    }
}