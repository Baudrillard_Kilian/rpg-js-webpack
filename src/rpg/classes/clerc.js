import { Caster } from '../stats/caster';

export class Clerc extends Caster {
    prayers = 1;

    heal(){
        console.log(`${this.name} se soigne`);
    };
    pray(){
        console.log(`${this.name} prie`);
    };
}