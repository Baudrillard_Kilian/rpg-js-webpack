import { Global } from '../stats/global';

export class Thief extends Global {
    agi = 8;
    armor = 3;
    speed = 7;
    str = 4;

    picklock(){
        console.log(`${this.name} crochete un coffre`);
    }
    steal(){
        console.log(`${this.name} vole un objet`);
    }
}