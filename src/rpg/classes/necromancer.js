import { Caster } from '../stats/caster';
import { Summon } from '../sorts/summon'

export class Necromancer extends Caster {
    invocation = new Summon;

    summon(){
        console.log(`${this.name} invoque ${this.invocation}`);
    }
}