import game from "../game";

export class Personnage {
    /*
    nom = 'Nom : ';
    arme = 'Arme : ';
    pv = 'Pv : ';
    mana = 'Mana : ';
    energie = 'Energie : ';
    armure = 'Armure : ';
    agiliter = 'Agiliter : ';
    vitesse = 'Vitesse : ';
    force = 'Force : ';
    priere = 'Priere de depart : ';
    */
    li(li) {
        const newDiv = document.createElement('div');
        const props = Object.getOwnPropertyNames( li );
        const newUl = document.createElement('ul');

        document.body.appendChild(newDiv);
        newDiv.appendChild(newUl);
        for(let prop of props) {
            let newLi = document.createElement('li');
            newLi.innerHTML = `${prop} : ${this[prop]}`;

            newUl.appendChild(newLi);
                console.log(prop);
        }
    }

    createWizzard(wizzard = game.wizzards){

        this.li(wizzard);
        console.log(wizzard);
        /*
        this.li(this.nom + wizzard.name);
        this.li(this.arme + wizzard.arme);
        this.li(this.pv + wizzard.hp);
        this.li(this.mana + wizzard.mana);
        this.li(this.energie + wizzard.stamina);
        this.li(this.armure + wizzard.armor);
        this.li(this.agiliter + wizzard.agi);
        this.li(this.vitesse + wizzard.speed);
        this.li(this.force + wizzard.str);
        */
    }

    createClerc(clerc){
        /*
        this.li(this.nom + clerc.name);
        this.li(this.arme + clerc.arme);
        this.li(this.pv + clerc.hp);
        this.li(this.mana + clerc.mana);
        this.li(this.energie + clerc.stamina);
        this.li(this.armure + clerc.armor);
        this.li(this.agiliter + clerc.agi);
        this.li(this.vitesse + clerc.speed);
        this.li(this.force + clerc.str);
        this.li(this.priere + clerc.prayers);
        */
    }

    createNecromancer(necromancer){

    }

    createWarrior(warrior){

    }

    createThief(thief){

    }
}
