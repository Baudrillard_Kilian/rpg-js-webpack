export class Weapon {
    //categorie des épées
    poignard = 'poignard';
    katana = 'katana';
    machette = 'machette';

    // categorie des objet magique
    sceptre = 'sceptre';
    grimoire = 'grimoire';

    // categorie des arme a distance
    arc = 'arc';
    couteauLancer = 'couteaux de lancer'
}