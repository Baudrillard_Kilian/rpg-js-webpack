import { Weapon } from '../armes/weapon';

export class Global {
    hp = 100;
    name;
    img;
    agi;
    armor;
    speed;
    stamina = 100;
    str;
    arme;
    weapon = new Weapon;

    attack(){
        console.log(`${this.name} attaque`)
    };
    block(){
        console.log(`${this.name} bloque l'attaque`)
    };
    dodge(){
        console.log(`${this.name} esquive le sort`)
    };
    walk(){
        console.log(`${this.name} marche`)
    };
}