import { Global } from './global';
import { Spell } from '../sorts/spell';
import { Cost} from '../sorts/manaCost';


export class Caster extends Global {
    mana = 100;
    spells = new Spell;
    cost = new Cost;
 
    agi = 3;
    armor = 2;
    speed = 3;
    str = 2;

    levitate(){
        // double l'agi et augmente la vitesse de 2
        console.log(`${this.name} commence a s'envoller`);
    }
    cast(){
        console.log(`${this.name} prepare sort ${this.spells}`)
    }
    endCast(){
        console.log(`${this.name} lance le sort ${this.spells}`)
    }
}